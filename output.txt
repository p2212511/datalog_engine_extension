*******************************************************
A(S2,Average):-Student(S1,S2,S3),Grade(S1,G2,G3),Avg(G3,S2,Average).
--------------------
( 'MarlonRobillard', 13.0)
( 'GustaveRiquier', 16.5)
( 'RogerLaprise', 15.0)
( 'DavidMorneau', 13.5)
( 'VadenSalois', 14.0)
( 'GiffordLamothe', 13.5)
