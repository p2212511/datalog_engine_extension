# Developing a Datalog Engine Extension for Aggregate Functions

The module's name is **engine**.

### Requirements

- Java 11 installed and configured.

### Code Repository

The code is available in the following url : [https://forge.univ-lyon1.fr/p2212511/datalog_engine_extension](https://forge.univ-lyon1.fr/p2212511/datalog_engine_extension)

### Build the project

To build the project, you need to run the following commands:

1. Open a command prompt or terminal.
2. Navigate to the project directory.
3. Run the following command:
```
mvn clean
```
4. Run the following command to install the project dependencies:
```
mvn install
```

### Run the project

To run the engine, perform the following steps:

1. Write the Datalog program in the **script.txt** file.
2. Launch the engine by executing the following command:
```
mvn -q exec:java
```
3. Check the result in the **output.txt** file.
