package org.engine.parsing;

import org.engine.model.Atom;
import org.engine.model.Attribute;
import org.engine.model.Edb;
import org.engine.model.Idb;
import org.engine.utils.Writer;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.*;

public class Parser {
    private final Catalog catalog = new Catalog();

    public void parse(BufferedReader reader) {
        // Read the input file: rule by rule
        try {
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.length() != 0) {
                    parseLine(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(4);
        }
    }

    private void parseLine(String line) {
        String output = line.replaceAll("\\s", "");
        // if the rule is an Edb creation
        if (output.contains("type")) {
            processEdb(output);
            // if the rule is an Idb declaration
        } else if (output.contains(":-")) {
            processRule(output);
            // if the rule is an Edb data insertion
        } else {
            processData(output);
        }
    }
    private void processEdb(String line) {
        // Compatible types
        Set<String> types = new HashSet<>();
        types.add("int");
        types.add("string");
        // Start parsing the rule
        try {
            // Remove type keyword and parentheses
            String output = line.substring(7, line.length() - 3);
            // Remove name of entity and parentheses
            String[] parts = output.split("\\(");
            // Check if the name of the Edb starts with a capital letter
            if (!Character.isUpperCase(parts[0].charAt(0))) {
                System.err.println("An Edb name must start with a capital letter at " + line);
                System.exit(1);
            }
            // Create new Edb
            Edb edb = new Edb();
            edb.setName(parts[0]);
            // Get the attributes with their type
            String attributesString = parts[1];
            String[] attributes = attributesString.split(",");
            int index = 0;
            // Check for type compatibility
            // else : An error is thrown and the program stops
            for (String item : attributes) {
                String[] attributeParts = item.split(":");
                if (!types.contains(attributeParts[1])) {
                    System.err.println("Invalid type at " + line);
                    System.exit(1);
                }
                edb.addAttribute(new Attribute(attributeParts[0], attributeParts[1], index));
                index++;
            }
            // Add edb to catalogue
            catalog.addEdb(edb.getName(), edb);
        } catch (Exception e) {
            System.err.println("syntax error at " + line);
            System.exit(1);
        }
    }

    private void processData(String line) {
        // Remove parentheses and the '.'
        String output = line.substring(0, line.length() - 2);
        // Remove name of Edb and parentheses
        String[] parts = output.split("\\(");
        // Check if the name of the Edb starts with a capital letter
        if (!Character.isUpperCase(parts[0].charAt(0))) {
            System.err.println("An Edb name must start with a capital letter at " + line);
            System.exit(1);
        }
        Edb edb = catalog.getEdb(parts[0]);
        // Extract data by column
        List<Attribute> edbTypes = edb.getAttributes();
        ArrayList<String> columns = new ArrayList<>();
        String[] data = parts[1].split(",");
        // Check parameter's type and number
        if (data.length != edbTypes.size()) {
            System.err.println("Invalid number of parameters at  " + line);
            System.exit(1);
        } else {
            int index = 0;
            // Check for type compatibility
            // else : An error is thrown and the program stops
            for (String item : data) {
                if (item.equals("null") || (item.chars().filter(c -> c == '\'').count() == 2 && edbTypes.get(index).getType().equals("string"))) {
                    columns.add(item);
                } else {
                    try {
                        Integer.parseInt(item);
                        columns.add(item);
                    } catch (Exception e) {
                        System.err.println("Invalid type for parameter " + index + " at line " + line);
                        System.exit(1);
                    }
                }

                index++;
            }
            // Add data to Edb
            edb.addData(columns);
        }
    }

    private void processRule(String line) {
        System.out.println("*******************************************************");
        System.out.println(line);
        System.out.println("--------------------");
        // Write to output file
        Writer.write("./output.txt", "*******************************************************\n");
        Writer.write("./output.txt", line);
        // Start parsing the rule
        try {
            // Get the head and the body of the rule
            // Remove the '.' in the end
            String output = line.substring(0, line.length() - 1);
            String[] parts = output.split(":-");
            // Check if the name of the predicat starts with a capital letter
            if (!Character.isUpperCase(parts[0].charAt(0))) {
                System.err.println("A predicat must start with a capital letter at " + line);
                System.exit(1);
            }
            String head = parts[0].substring(0, parts[0].length() - 1);
            // Get the attributes of the head atom
            String[] headParts = head.split("\\(");
            String[] headAttributes = headParts[1].split(",");
            // Create a new IDB
            Idb idb = new Idb();
            // Store the attributes in the head atom
            idb.setName(headParts[0]);
            for (String item : headAttributes) {
                idb.addSecondaryAttribute(item);
            }
            // Get the body atoms
            String body = parts[1].substring(0, parts[1].length() - 1);
            String[] bodyParts = body.split("\\),");
            // For each body atom process it
            for (String item : bodyParts) {
                Atom atom = processAtom(item);
                // Add the processed body atom to the head
                idb.addAtom(atom);
            }
            // Evaluate the idb
            idb.evaluate();
            // Add the Idb to the catalogue
            catalog.addIdb(idb.getName(), idb);
            // Write the evaluation result of the rule to the output file
            Writer.write("./output.txt", "\n--------------------\n");
            Writer.write("./output.txt", idb.toString());
        } catch (Exception e) {
            System.err.println("syntax error at " + line);
            System.exit(1);
        }
    }

    public Atom processAtom(String predicat) {
        if (!Character.isUpperCase(predicat.charAt(0))) {
            System.err.println("A predicat must start with a capital letter at " + predicat);
            System.exit(1);
        }
        // Get the predicat name
        String[] predicatParts = predicat.split("\\(");
        // Check if the atom is already defined as an Idb or Edb
        Edb edb = catalog.getEdb(predicatParts[0]);
        Idb idb = catalog.getIdb(predicatParts[0]);
        // Get the list of attributes
        String[] attributesNames = (predicatParts[1]).split(",");
        // If the atom is already defined as an Edb
        if (edb != null) {
            // Create a new Idb
            Idb newIdb = new Idb();
            newIdb.setName(edb.getName());
            // Copy the existing Edb attributes and data to the new created Edb
            for (int i = 0; i < attributesNames.length; i++) {
                Attribute newAttribute = edb.getAttribute(i);
                newAttribute.setName(attributesNames[i]);
                newIdb.addSecondaryAttribute(newAttribute.getName());
            }
            for (int i = 0; i < edb.getDataSize(); i++) {
                ArrayList<String> data = new ArrayList<>();

                for (int j = 0; j < attributesNames.length; j++) {
                    data.add(edb.getTupleColumn(i, j));
                }
                newIdb.addData(data);
            }
            return newIdb;

        } else if (idb != null)
        // If the atom is already defined as an Idb
        {
            idb.clearAttributes();
            idb.setSecondaryAttributes(new ArrayList<>(Arrays.asList(attributesNames)));
            return idb;
        } else {
            Idb newIdb = new Idb();
            newIdb.setName(predicatParts[0]);
            for (String attributesName : attributesNames) {
                newIdb.addSecondaryAttribute(attributesName);
            }
            return newIdb;
        }
    }
}
