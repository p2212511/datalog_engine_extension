package org.engine.parsing;

import org.engine.model.Edb;
import org.engine.model.Idb;

import java.util.HashMap;
import java.util.Map;

public class Catalog {
    private final Map<String, Edb> edbs = new HashMap<>();
    private final Map<String, Idb> idbs = new HashMap<>();

    public void addEdb(String name, Edb edb) {
        edbs.put(name, edb);
    }

    public void addIdb(String name, Idb idb) {
        idbs.put(name, idb);
    }

    public Edb getEdb(String name) {
        return edbs.get(name);
    }

    public Idb getIdb(String name) {
        return idbs.get(name);
    }
}
