package org.engine;

import org.engine.parsing.Parser;
import org.engine.utils.Reader;
import org.engine.utils.Writer;

public class Main {
    public static void main(String[] args) {
        Writer.create("./output.txt");
        long startTime = System.currentTimeMillis();
        var reader = Reader.read("./script.txt");
        Parser parser = new Parser();
        parser.parse(reader);
        long endTime = System.currentTimeMillis();
        long timeElapsed = endTime - startTime;
        System.out.println("Total execution time: " + timeElapsed + " ms");
    }
}