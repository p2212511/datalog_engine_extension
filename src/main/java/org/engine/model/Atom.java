package org.engine.model;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public abstract class Atom {
    protected String name;
    protected final List<List<String>> data = new ArrayList<>();

    public void addData(List<String> data) {
        this.getData().add(data);
    }

    public int getDataSize() {
        return data.size();
    }

    private List<String> getTuple(int index) {
        return data.get(index);
    }

    public String getTupleColumn(int line, int column) {
        return getTuple(line).get(column);
    }

    abstract List<String> getAttributesNames();
}
