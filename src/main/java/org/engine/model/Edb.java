package org.engine.model;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class Edb extends Atom {
    private final List<Attribute> attributes = new ArrayList<>();

    @Override
    public List<String> getAttributesNames() {
        ArrayList<String> attributesNames = new ArrayList<>();
        for (Attribute attribute : attributes) {
            attributesNames.add(attribute.getName());
        }
        return attributesNames;
    }

    public Attribute getAttribute(int index) {
        return attributes.get(index);
    }

    public void addAttribute(Attribute attribute) {
        attributes.add(attribute);
    }

}
