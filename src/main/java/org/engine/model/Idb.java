package org.engine.model;

import lombok.Getter;
import lombok.Setter;

import java.util.*;

@Setter
@Getter
public class Idb extends Atom {
    private List<Atom> atoms = new ArrayList<>();
    private List<String> secondaryAttributes = new ArrayList<>();

    public void addSecondaryAttribute(String secondaryAttribute) {
        this.secondaryAttributes.add(secondaryAttribute);
    }

    public void addAtom(Atom atom) {
        this.atoms.add(atom);
    }

    public void clearAttributes() {
        this.atoms.clear();
    }

    public void evaluate() {
        Atom result = null;
        // Evaluate the body of the rule
        if (atoms.size() == 1) {
            var atom = this.filter(atoms.get(0), atoms.get(0).getAttributesNames());
            atoms.set(0, atom);
            result = atoms.get(0);
        } else {
            // Evaluate atom1,atom2
            for (int i = 0; i < atoms.size() - 1; i++) {
                Atom atom1;
                if (result == null) {
                    atom1 = atoms.get(i);
                } else {
                    atom1 = result;
                }
                var atom2 = atoms.get(i + 1);
                List<String> atom1Attributes = atom1.getAttributesNames();
                List<String> atom2Attributes = atom2.getAttributesNames();
                // Filter tuples if there is a filter condition
                atom1 = filter(atom1, atom1Attributes);
                atom1Attributes = atom1.getAttributesNames();
                switch (atom2.name) {
                    case "Count":
                        result = processAggregate(atom1, atom1Attributes, atom2, atom2Attributes, "Count");
                        break;
                    case "Avg":
                        try {
                            result = processAggregate(atom1, atom1Attributes, atom2, atom2Attributes, "Avg");
                        } catch (NumberFormatException e) {
                            System.err.println("Aggregate function Avg can't be performed on none type int");
                            System.exit(1);
                        }
                        break;
                    case "Sum":
                        try {
                            result = processAggregate(atom1, atom1Attributes, atom2, atom2Attributes, "Sum");
                        } catch (NumberFormatException e) {
                            System.err.println("Aggregate function Sum can't be performed on none type int");
                            System.exit(1);
                        }
                        break;
                    case "Max":
                        try {
                            result = processAggregate(atom1, atom1Attributes, atom2, atom2Attributes, "Max");
                        } catch (NumberFormatException e) {
                            System.err.println("Aggregate function Max can't be performed on none type int");
                            System.exit(1);
                        }
                        break;
                    case "Min":
                        try {
                            result = processAggregate(atom1, atom1Attributes, atom2, atom2Attributes, "Min");
                        } catch (NumberFormatException e) {
                            System.err.println("Aggregate function Min can't be performed on none type int");
                            System.exit(1);
                        }
                        break;

                    default:
                        atom2 = filter(atom2, atom2Attributes);
                        atom2Attributes = atom2.getAttributesNames();
                        if (Collections.disjoint(atom1Attributes, atom2Attributes)) {
                            // If it's a jointure without condition : tuples(atom1) x tuples(atom2)
                            result = processJointureWithoutCondition(atom1, atom2);
                            /*Build a result with:
                            data = tuples(atom1) x tuples(atom2)
                            attributes = attributes(atom1) + attributes(atom2)
                            */
                        } else {
                            // If it's a jointure with condition
                            result = processJointureWithCondition(atom1, atom1Attributes, atom2, atom2Attributes);
                            /*Build a result with:
                            data = tuples(atom1) x tuples(atom2)
                            attributes = attributes(atom1) + attributes(atom2)
                            */
                        }
                        result.getAttributesNames().addAll(atom1Attributes);
                        result.getAttributesNames().addAll(atom2Attributes);
                        break;
                }
            }
        }
        // Projection
        project(result);
    }

    private Atom processJointureWithoutCondition(Atom atom1, Atom atom2) {
        String name = (atom1.name == null ? "RES" : atom1.getName());
        System.out.print("join without condition between " + name + " and " + atom2.getName());
        long startTime = System.nanoTime();
        Idb atom = new Idb();
        for (var item1 : atom1.getData()) {
            ArrayList<String> resultData = new ArrayList<>(item1);
            ArrayList<String> copyData = new ArrayList<>(resultData);
            for (var item2 : atom2.getData()) {
                resultData = new ArrayList<>(copyData);
                resultData.addAll(item2);
                atom.addData(resultData);
            }
        }
        long endTime = System.nanoTime();
        long timeElapsed = endTime - startTime;
        System.out.println("(" + timeElapsed + " ns)");
        return atom;
    }

    private Atom processJointureWithCondition(Atom atom1, List<String> atom1Attributes, Atom atom2, List<String> atom2Attributes) {
        String name = (atom1.name == null ? "RES" : atom1.getName());
        System.out.print("Join with condition between " + name + " and " + atom2.getName());
        long startTime = System.nanoTime();
        Atom atom = new Idb();
        // Find the jointure conditions
        List<JoinCondition> joinConditions = new ArrayList<>();
        for (int j = 0; j < atom1Attributes.size(); j++) {
            for (int k = 0; k < atom2Attributes.size(); k++) {
                if (atom1Attributes.get(j).equals(atom2Attributes.get(k))) {
                    joinConditions.add(new JoinCondition(j, k));
                }
            }
        }
        // Take only tuples which satisfy the jointure condition
        for (int j = 0; j < atom1.getDataSize(); j++) {
            ArrayList<String> resultData = new ArrayList<>(atom1.data.get(j));
            ArrayList<String> copyData = new ArrayList<>(resultData);
            for (int k = 0; k < atom2.getDataSize(); k++) {
                boolean respect = true;
                for (JoinCondition condition : joinConditions) {
                    if (!atom1.getTupleColumn(j, condition.getIndex1()).equals(atom2.getTupleColumn(k, condition.getIndex2()))) {
                        respect = false;
                        break;
                    }
                }
                if (respect) {
                    resultData = new ArrayList<>(copyData);
                    resultData.addAll(atom2.data.get(k));
                    atom.data.add(resultData);
                }
            }
        }
        long endTime = System.nanoTime();
        long timeElapsed = endTime - startTime;
        System.out.println("(" + timeElapsed + " ns)");
        return atom;
    }

    public Atom processAggregate(Atom atom1, List<String> atom1Attributes, Atom atom2, List<String> atom2Attributes, String operation) {
        if (atom2Attributes.size() != 3) {
            System.err.println("Invalid aggregate function " + operation + "at " + atom2.getName());
            System.exit(1);
        } else {
            // Find the distinct value of the aggregate attribute
            int indexAggregate = 0;
            int indexGroup = 0;
            for (int i = 0; i < atom1Attributes.size(); i++) {
                if (atom2.getAttributesNames().get(0).equals(atom1.getAttributesNames().get(i))) {
                    indexAggregate = i;
                }
                if (atom2.getAttributesNames().get(1).equals(atom1.getAttributesNames().get(i))) {
                    indexGroup = i;
                }
            }
            // Find the distinct value of the group by attribute
            Set<String> distinctValuesGroup = new HashSet<>();
            for (int i = 0; i < atom1.getDataSize(); i++) {
                if (!atom1.getTupleColumn(i, indexGroup).equals("null"))
                    distinctValuesGroup.add(atom1.getTupleColumn(i, indexGroup));
            }
            Atom atom = null;
            switch (operation) {
                case "Count":
                    atom = processCountAggregate(atom1, atom2, indexAggregate, indexGroup, distinctValuesGroup);
                    break;
                case "Avg":
                    atom = processAvgAggregate(atom1, atom2, indexAggregate, indexGroup, distinctValuesGroup);
                    break;
                case "Sum":
                    atom = processSumAggregate(atom1, atom2, indexAggregate, indexGroup, distinctValuesGroup);
                    break;
                case "Max":
                    atom = processMaxAggregate(atom1, atom2, indexAggregate, indexGroup, distinctValuesGroup);
                    break;
                case "Min":
                    atom = processMinAggregate(atom1, atom2, indexAggregate, indexGroup, distinctValuesGroup);
                    break;
            }
            return atom;
        }
        return null;
    }

    private Atom processCountAggregate(Atom atom1, Atom atom2, int indexAggregate, int indexGroup, Set<String> distinctValuesGroup) {
        System.out.print("Count aggregate operation");
        long startTime = System.nanoTime();
        Idb idb = new Idb();
        // Count for each distinct value
        for (String distinctValueGroup : distinctValuesGroup) {
            ArrayList<String> result = new ArrayList<>();
            HashSet<String> distinctValuesCount = new HashSet<>();
            for (int i = 0; i < atom1.getDataSize(); i++) {
                if (atom1.getTupleColumn(i, indexGroup).equals(distinctValueGroup) && !atom1.getTupleColumn(i, indexAggregate).equals("null")) {
                    distinctValuesCount.add(atom1.getTupleColumn(i, indexAggregate));
                }
            }
            result.add(distinctValueGroup);
            result.add(String.valueOf(distinctValuesCount.size()));
            idb.addData(result);
        }
        ArrayList<String> newAttributes = new ArrayList<>(atom2.getAttributesNames());
        newAttributes.remove(0);
        idb.setSecondaryAttributes(newAttributes);
        long endTime = System.nanoTime();
        long timeElapsed = endTime - startTime;
        System.out.println("(" + timeElapsed + " ns)");
        return idb;
    }

    private Idb processAvgAggregate(Atom atom1, Atom atom2, int indexAggregate, int indexGroup, Set<String> distinctValuesGroup) {
        System.out.print("Avg aggregate operation");
        long startTime = System.nanoTime();
        Idb idb = new Idb();
        // Sum for each distinct value
        for (String distinctValueGroup : distinctValuesGroup) {
            ArrayList<String> result = new ArrayList<>();
            double sum = 0.0;
            double nbElement = 0.0;
            for (int i = 0; i < atom1.getDataSize(); i++) {
                if (atom1.getTupleColumn(i, indexGroup).equals(distinctValueGroup) && !atom1.getTupleColumn(i, indexAggregate).equals("null")) {
                    sum = sum + Double.parseDouble(atom1.data.get(i).get(indexAggregate));
                    nbElement++;
                }
            }

            if (nbElement != 0.0) {
                result.add(distinctValueGroup);
                result.add(String.valueOf(sum / nbElement));
                idb.data.add(result);
            }

        }
        ArrayList<String> newAttributes = new ArrayList<>(atom2.getAttributesNames());
        newAttributes.remove(0);
        idb.setSecondaryAttributes(newAttributes);
        long endTime = System.nanoTime();
        long timeElapsed = endTime - startTime;
        System.out.println("(" + timeElapsed + " ns)");
        return idb;
    }

    private Idb processSumAggregate(Atom atom1, Atom atom2, int indexAggregate, int indexGroup, Set<String> distinctValuesGroup) {
        System.out.print("Sum aggregate operation");
        long startTime = System.nanoTime();
        Idb idb = new Idb();
        // Sum for each distinct value
        for (String distinctValueGroup : distinctValuesGroup) {
            ArrayList<String> result = new ArrayList<>();
            double sum = 0.0;
            for (int i = 0; i < atom1.getDataSize(); i++) {
                if (atom1.getTupleColumn(i, indexGroup).equals(distinctValueGroup) && !atom1.getTupleColumn(i, indexAggregate).equals("null")) {
                    sum = sum + Double.parseDouble(atom1.data.get(i).get(indexAggregate));
                }
            }
            result.add(distinctValueGroup);
            result.add(String.valueOf(sum));
            idb.data.add(result);

        }
        ArrayList<String> newAttributes = new ArrayList<>(atom2.getAttributesNames());
        newAttributes.remove(0);
        idb.setSecondaryAttributes(newAttributes);
        long endTime = System.nanoTime();
        long timeElapsed = endTime - startTime;
        System.out.println("(" + timeElapsed + " ns)");
        return idb;
    }

    private Idb processMaxAggregate(Atom atom1, Atom atom2, int indexAggregate, int indexGroup, Set<String> distinctValuesGroup) {
        System.out.print("Max aggregate operation");
        long startTime = System.nanoTime();
        Idb idb = new Idb();
        // Sum for each distinct value
        for (String distinctValueGroup : distinctValuesGroup) {
            ArrayList<String> result = new ArrayList<>();
            double max = Integer.MIN_VALUE;
            for (int i = 0; i < atom1.getDataSize(); i++) {
                if (atom1.getTupleColumn(i, indexGroup).equals(distinctValueGroup) && !atom1.getTupleColumn(i, indexAggregate).equals("null")) {
                    double value = Double.parseDouble(atom1.data.get(i).get(indexAggregate));
                    if (value > max) {
                        max = value;
                    }
                }
            }
            result.add(distinctValueGroup);
            result.add(String.valueOf(max));
            idb.data.add(result);
        }
        ArrayList<String> newAttributes = new ArrayList<>(atom2.getAttributesNames());
        newAttributes.remove(0);
        idb.setSecondaryAttributes(newAttributes);
        long endTime = System.nanoTime();
        long timeElapsed = endTime - startTime;
        System.out.println("(" + timeElapsed + " ns)");
        return idb;
    }

    private Idb processMinAggregate(Atom atom1, Atom atom2, int indexAggregate, int indexGroup, Set<String> distinctValuesGroup) {
        System.out.print("Min aggregate operation");
        long startTime = System.nanoTime();
        Idb idb = new Idb();
        // Sum for each distinct value
        for (String distinctValueGroup : distinctValuesGroup) {
            ArrayList<String> result = new ArrayList<>();
            double min = Integer.MAX_VALUE;
            for (int i = 0; i < atom1.getDataSize(); i++) {
                if (atom1.getTupleColumn(i, indexGroup).equals(distinctValueGroup) && !atom1.getTupleColumn(i, indexAggregate).equals("null")) {
                    double value = Double.parseDouble(atom1.data.get(i).get(indexAggregate));
                    if (value < min) {
                        min = value;
                    }
                }
            }
            result.add(distinctValueGroup);
            result.add(String.valueOf(min));
            idb.data.add(result);
        }
        ArrayList<String> newAttributes = new ArrayList<>(atom2.getAttributesNames());
        newAttributes.remove(0);
        idb.setSecondaryAttributes(newAttributes);
        long endTime = System.nanoTime();
        long timeElapsed = endTime - startTime;
        System.out.println("(" + timeElapsed + " ns)");
        return idb;
    }

    private Atom filter(Atom atom, List<String> atomAttributes) {
        String name = (atom.name == null ? "RES " : atom.getName());
        System.out.print("Filter " + name);
        long startTime = System.nanoTime();
        Idb newIdb = new Idb();
        // Copy the existing Edb attributes and data to the new created Idb
        ArrayList<FilterCondition> filterConditions = new ArrayList<>();
        for (int i = 0; i < atomAttributes.size(); i++) {
            newIdb.getSecondaryAttributes().add(atomAttributes.get(i));
            if (!Character.isUpperCase(atomAttributes.get(i).charAt(0))) {
                filterConditions.add(new FilterCondition(i, atomAttributes.get(i)));
            }
        }
        for (int i = 0; i < atom.getData().size(); i++) {
            boolean respect = true;
            ArrayList<String> data = new ArrayList<>();
            for (FilterCondition condition : filterConditions) {
                if (!atom.getTupleColumn(i, condition.getIndex()).equals(condition.getValue())) {
                    respect = false;
                    break;
                }
            }
            if (respect) {
                for (int j = 0; j < atomAttributes.size(); j++) {
                    data.add(atom.getTupleColumn(i, j));
                }
            }
            if (!data.isEmpty()) newIdb.addData(data);
        }
        newIdb.setName(atom.getName());
        long endTime = System.nanoTime();
        long timeElapsed = endTime - startTime;
        System.out.println("(" + timeElapsed + " ns)");
        return newIdb;
    }

    private void project(Atom result) {
        if (result.getName() != null) {
            System.out.print("Projection " + result.getName());
        } else {
            System.out.print("Projection ");
        }
        long startTime = System.nanoTime();
        for (int i = 0; i < result.getDataSize(); i++) {
            ArrayList<String> tuple = new ArrayList<>();
            for (String secondaryAttribute : getSecondaryAttributes()) {
                int index = 0;
                for (var attribute : result.getAttributesNames()) {
                    if (secondaryAttribute.equals(attribute)) {
                        tuple.add(result.getTupleColumn(i, index));
                    }
                    index++;
                }
            }
            if (tuple.size() != 0) this.addData(tuple);
        }
        long endTime = System.nanoTime();
        long timeElapsed = endTime - startTime;
        System.out.println("(" + timeElapsed + " ns)");
    }

    @Override
    public String toString() {
        try {
            StringBuilder builder = new StringBuilder();
            if (data.size() > 0) {
                for (List<String> datum : getData()) {
                    builder.append("(");
                    for (int j = 0; j < datum.size(); j++) {
                        builder.append(" ").append(datum.get(j));
                        if (j != datum.size() - 1) {
                            builder.append(",");
                        }
                    }
                    builder.append(")\n");
                }
            } else {
                builder.append("null\n");
            }
            return builder.toString();
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Invalid attribute projection");
            System.exit(1);
        }
        return null;
    }

    @Override
    List<String> getAttributesNames() {
        return secondaryAttributes;
    }
}
