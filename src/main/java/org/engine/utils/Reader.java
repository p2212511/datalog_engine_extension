package org.engine.utils;

import java.io.*;

public class Reader {
    public static BufferedReader read(String path) {
        FileInputStream fis;
        BufferedInputStream bis;
        InputStreamReader isr;
        BufferedReader br = null;
        try {
            fis = new FileInputStream(path);
            bis = new BufferedInputStream(fis);
            isr = new InputStreamReader(bis);
            br = new BufferedReader(isr);
        } catch (RuntimeException | IOException e) {
            e.printStackTrace();
            System.exit(4);
        }
        return br;
    }
}
