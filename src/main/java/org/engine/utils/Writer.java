package org.engine.utils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Writer {
    public static void write(String path, String result) {
        try {
            FileWriter fileWriter = new FileWriter(path, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            bufferedWriter.write(result);
            bufferedWriter.flush();
            bufferedWriter.close();
        } catch (RuntimeException | IOException e) {
            e.printStackTrace();
            System.exit(4);
        }
    }

    public static void create(String path) {
        try {
            new FileWriter(path, false);
        } catch (RuntimeException | IOException e) {
            e.printStackTrace();
            System.exit(4);
        }
    }
}
